package com.example.task1;

import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.google.android.material.navigation.NavigationView;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    DrawerLayout drawer;
    NavigationView navigationView;
    Toolbar toolbar;
    ActionBarDrawerToggle toggle;
    List<Items> itemsList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        toolbar = findViewById(R.id.toolbarID);
        navigationView = findViewById(R.id.navigationID);
        drawer = findViewById(R.id.drawerID);
        ImageView imageView = findViewById(R.id.img1ID);

        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (drawer.isDrawerOpen(GravityCompat.START)) {
                    drawer.closeDrawer(GravityCompat.START);
                }
                else
                {
                    drawer.openDrawer(GravityCompat.START);
                }

            }
        });

      //  setSupportActionBar(toolbar);
        toggle = new ActionBarDrawerToggle(this, drawer,toolbar, R.string.open,R.string.close);
        drawer.addDrawerListener(toggle);
       // toggle.syncState();

        itemsList = new ArrayList<>();
        itemsList.add(new Items("Gardening","Description",R.drawable.ic_gardening));
        itemsList.add(new Items("Crops","Description",R.drawable.ic_crops));
        itemsList.add(new Items("Products","Description",R.drawable.ic_gardening));
        itemsList.add(new Items("Market Place","Description",R.drawable.ic_market_place));
        itemsList.add(new Items("Gallery","Description",R.drawable.ic_gallery));
        itemsList.add(new Items("Article","Description",R.drawable.ic_article));

        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recyclerID);
        RecyclerViewAdapter myAdapter = new RecyclerViewAdapter(this, itemsList);
        recyclerView.setLayoutManager(new GridLayoutManager(this, 2));
        recyclerView.setAdapter(myAdapter);

    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        }

        else{
            super.onBackPressed();
        }
    }
}